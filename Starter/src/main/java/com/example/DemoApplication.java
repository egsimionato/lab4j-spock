package com.example;

import com.example.Greeting;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.inject.Inject;

@SpringBootApplication
public class DemoApplication {

    @Inject
    private Greeting greeting;

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
        System.out.println("Matilda :::::" + greeting.getName());
	}
}
