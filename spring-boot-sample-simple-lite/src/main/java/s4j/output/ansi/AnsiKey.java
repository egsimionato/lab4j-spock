package s4j.output.ansi;

public enum AnsiKey {

    CHECK_MARK("\u2713"),
    ERROR_MARK("\u2717");

    private String code;

    AnsiKey(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public String toString() {
        return code;
    }
}
