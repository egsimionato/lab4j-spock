package com.manning.spock.chapter1;

import spock.lang.*;

class AdderSpec extends spock.lang.Specification {

    def "Adding two numbers to return the sum"() {
        when: "a new Adder class is created"
        def adder = 1;

        then: "1 plus 1 is 2"
        adder == 2;
    }


}
