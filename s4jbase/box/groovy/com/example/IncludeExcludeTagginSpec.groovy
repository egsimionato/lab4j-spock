import java.lang.annotation.*

class IncludeExcludeTagginSpec extends spock.lang.Specification {

    static {
        //System.setProperty "spock.configuration", "IncludeFastConfig.groovy"
        System.setProperty "spock.configuration", "ExcludeSlowConfig.groovy"
    }

    @Fast
    def "xD fast method" () {
        expect: true
    }

    @Slow
    def "xD slow method" () {
        expect: true
    }

    def "xD neither fast nor slow method" () {
        expect: true
    }
}

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@interface Fast {}

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@interface Slow {}
