import groovy.sql.Sql
import spock.lang.Shared

class DatabaseDrivenSpec extends spock.lang.Specification {

    @Shared sql = Sql.newInstance("jdbc:h2:mem:", "org.h2.Driver")

    def setupSpec() {
        sql.execute("create table maxdata (id int primary key, a int, b int, c int)")
        sql.execute("create table mindata (id int primary key, a int, b int, c int)")
        sql.execute("insert into maxdata values (1, 3, 7, 7), (2, 5, 4, 5), (3, 9, 9, 9)")
        sql.execute("insert into mindata values (1, 3, 7, 3), (2, 5, 4, 4), (3, 9, 9, 9)")
    }

    def "maximum of two numbers" () {
        expect:
            Math.max(a, b) == c

        where:
            [a, b, c] << sql.rows("select a, b, c from maxdata")
    }

    def "minimum of two numbers" () {
        expect:
            Math.min(a, b) == c

        where:
            [a, b, c] << sql.rows("select a, b, c from mindata")
    }


}
