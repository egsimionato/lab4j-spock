
class MultipleThenSpec extends spock.lang.Specification {

    def "collaborators must be invoked in order" () {
        def coll1 = Mock(Collaborator)
        def coll2 = Mock(Collaborator)

        when:
            coll1.collaborate()
            coll2.collaborate()

        then:
        1 * coll1.collaborate()

        then:
        1 * coll2.collaborate()
    }
}

interface Collaborator {
    def collaborate()
}

