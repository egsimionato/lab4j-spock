package sample.simple;

import sample.simple.service.HelloWorldService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.env.Environment;
import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import s4j.output.ansi.AnsiColor;
import s4j.output.ansi.AnsiKey;

@SpringBootApplication
public class SampleSimpleApplication implements CommandLineRunner {

    private static final Logger log = LoggerFactory.getLogger(SampleSimpleApplication.class);

    @Inject
    private Environment env;

	@Autowired
	private HelloWorldService helloWorldService;

	@Override
	public void run(String... args) {
        System.out.println(AnsiColor.GREEN + "----------------------------------------------------------\n\t" +
                " > Application '{}' is running! \n" +
                "----------------------------------------------------------" + AnsiColor.RESET);
        System.out.println(AnsiColor.GREEN + "> ["+AnsiKey.CHECK_MARK+"] hello_message: " + helloWorldService.getHelloMessage() + AnsiColor.RESET);
        System.out.println(AnsiColor.RED + "> ["+AnsiKey.ERROR_MARK+"] hello_message: " + helloWorldService.getHelloMessage() + AnsiColor.RESET);
		if (args.length > 0 && args[0].equals("exitcode")) {
			throw new ExitException();
		}
	}

	public static void main(String[] args) throws Exception {
		SpringApplication.run(SampleSimpleApplication.class, args);
	}

}
