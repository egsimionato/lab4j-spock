Groovy
======

Groovy vs Java
-------------
* Is a dynamic language (java is static)
* Is a strongly typed language (same as Java)
* Is object-oriented (same as Java)
* Comes with the GDK (java has the JDK)
* Runs on the JVM (same as java)
* Favors concise code (java is considered verbose compared to Groovy)
* Offers its own libraries (for example, web and object relational frameworks)
* Can use any existing Java library as-is (Java can also call Groovy code)
* Has closures (java 8 has lambda expresions)
* Supports duck typing (java has strict inheritance)

Groovy Essential
----------------

Groovy Useful Features
----------------------

Groovy Advanced
---------------

Groovy Advanced Features
------------------------

