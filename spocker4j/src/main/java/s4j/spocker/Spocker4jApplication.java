package s4j.spocker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Spocker4jApplication {

	public static void main(String[] args) {
		SpringApplication.run(Spocker4jApplication.class, args);
	}
}
